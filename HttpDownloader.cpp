#include "HttpDownloader.h"
#include "os.h"
#include <string>
#include <iostream>
#include <map>

HttpDownloader::HttpDownloader()
{

	url="";
	service = "", resource="";
	
}

void HttpDownloader::Download(std::string url)
{
	response_strings.clear();
	this->url = url;
	parseURL(url);
	if(host == "" || resource == "" || service == "")
	{
		std::cout << "Invalid URL: " << url << std::endl;
		return;
	}
	else if(port == "")
	{
		port = "80";
	}
	if(service != "http")
	{
		std::cout << "This program does not support \"" << service << "\""<< std::endl;
		return;
	}
	win_dl(this);
	parseHTTP();
}

void HttpDownloader::parseURL(std::string url)
{
	bool done = false;

	while(!done){
	
		uint32_t i=0, service_end=0;
		for(i=0; i < url.length(); i++)
		{
			if(url[i] == ':' && !(i+2 >= url.length()) && url[i+1] == '/' && url[i+2] == '/')
			{
					service = url.substr(0,i);
					i+=3;
					service_end = i;
					break;

			}
		}
		for(; i < url.length(); i++)
		{
			if(url[i] == '/')
			{
				host = url.substr(service_end,i-service_end);
				resource = url.substr(i, url.length()-1);
				done=true;
				break;
			}
			if(i+1<url.length()&&url[i]==':'&&(url[i+1]>='0'&&url[i+1]<='9'))
			{
				size_t j=i+2;
				while(j<url.length())
				{
					if((url[j] >= '0' && url[j] <= '9'))
					{
						j++;
					}
					else
						break;
					
				}
				port = url.substr(i+1, j-i-1);

			}
			if(i+1 == url.length())
			{
				host = url.substr(service_end,i-service_end+1);
				resource = '/';
				done = true;
				break;
			}
		}
		done = true;
	}
	
}

void HttpDownloader::parseHTTP()
{
	std::string res = "", headers_string = "", content="";
	
	for (size_t i=0;i<response_strings.size();i++)
	{
		res += response_strings.at(i);

	}
	bool done = false;
	while(!done)
	{
		for(size_t i =0; i < res.length();i++)
		{
			if(i+3 < res.length() && res[i] == '\r' && res[i+1] == '\n' && res[i+2] == '\r' && res[i+3] == '\n')
			{
				headers_string = res.substr(0, i+2);
 				std::map<std::string, std::string> headers = headersToMap(headers_string);
				if(headers["status"] == "301" || headers["status"] == "302")
				{
					std::cout << "Redirecting to " << headers["Location"] << std::endl;
					Download(headers["Location"]);
				}
				content = res.substr(i+4, res.length()-i);
				break;
			}

			
		}
		done=true;
	}
	return;
}

std::map<std::string,std::string> HttpDownloader::headersToMap(std::string headers_strings)
{
	std::map<std::string, std::string> headers;
	std::vector<std::string> header_lines;
	std::string status="";
	size_t last_line_end=0;
	for(size_t i=0; i<headers_strings.length();i++)
	{
		if(i+1 < headers_strings.length() &&headers_strings[i] == '\r' && headers_strings[i+1] == '\n')
		{
			header_lines.push_back(headers_strings.substr(last_line_end, i-last_line_end));
			last_line_end = i+2;
			i+=2;
		}
	}
	for(size_t i=0;i<header_lines.size(); i++)
	{
		std::string ch = header_lines.at(i);
		for(size_t j=0;j<ch.length();j++)
		{
			if(i+3 < ch.length() && ch[j] == 'H' && ch[j+1] == 'T' && ch[j+2] == 'T' && ch[j+3] == 'P')
			{
				for(size_t k=0; k < ch.length(); k++)
				{
					if(k+2<ch.length()&&ch[k]==' '&&(ch[k+1]>='0'&&ch[k+1]<='9')&&(ch[k+1]>='0'&&ch[k+2]<='9')&&(ch[k+3]>='0'&&ch[k+1]<='9'))
					{
						status = ch.substr(k+1, 3);
						break;
					}
				}
				continue;
			}
			if(j+1 < ch.length() && ch[j] == ':' && ch[j+1] == ' ')
			{
				std::string left="", right="";
				left = ch.substr(0,j);
				right = ch.substr(j+2,ch.length()-2);
				headers[left] = right;
				continue;
			}
		}
	}
		headers["status"] = status;
		return headers;
}


std::string HttpDownloader::getPort()
{
	return port;
}
std::string HttpDownloader::getService()
{
	return service;

}
std::string HttpDownloader::getResource()
{
	return resource;
}
std::string HttpDownloader::getHost()
{
	return host;
}
HttpDownloader::~HttpDownloader(void)
{
}
