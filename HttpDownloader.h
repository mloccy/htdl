#ifndef _HTTPCLIENT_H
#define _HTTPCLIENT_H
#include "os.h"
#include <string>
#include <vector>
#include <map>
class HttpDownloader
{
public:
	HttpDownloader();
	~HttpDownloader(void);
	std::string getService();
	std::string getResource();
	std::string getHost();
	std::string getURL();
	std::string getPort();
	void Download(std::string url);
private:
	std::map<std::string, std::string> headersToMap(std::string headers);
	void parseURL(std::string url);
	void parseHTTP();
	struct addrinfo * mHostInfo;
	std::string service, host, resource, url, port;
	std::vector<std::string> response_strings;
	#ifdef _WIN32
		friend extern int win_dl(HttpDownloader * client);
	#endif


};

#endif

