#include "os.h"
#include "HttpDownloader.h"
#include <iostream>
int win_dl(HttpDownloader * client)
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
    int iResult=0;
	    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) 
	{
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
	iResult = getaddrinfo(client->getHost().c_str(), client->getPort().c_str(), &hints, &result);

    if ( iResult != 0 ) 
	{
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }
	client->mHostInfo = result;

	// Attempt to connect to an address until one succeeds
	for(ptr=client->mHostInfo; ptr != NULL ;ptr=ptr->ai_next) 
	{

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) 
		{
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) 
		{
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    

    if (ConnectSocket == INVALID_SOCKET) 
	{
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }
	char * fmt = "GET %s HTTP/1.1\r\nHost: %s\r\nUser-Agent: cvize-downloader\r\n\r\n";
	size_t buflen = strlen(fmt)+ strlen(client->host.c_str()) + strlen(client->resource.c_str())-3;
	char * request_buf = (char *)malloc(buflen);
	sprintf(request_buf, fmt, client->resource.c_str(), client->host.c_str());
	iResult = send(ConnectSocket, request_buf, (int)strlen(request_buf), 0);
	if (iResult == SOCKET_ERROR) 
	{
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
	iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) 
	{
		printf("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
    int recvbuflen = 20480;
	//char * recvbuf =(char*)malloc(recvbuflen);
	char recvbuf[20480];

	do 
	{
		ZeroMemory(&recvbuf, recvbuflen);
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        if ( iResult > 0 )
		{

            //printf("Bytes received: %d\n", iResult);
			client->response_strings.push_back(recvbuf);
			//printf("%s\n", recvbuf);
		}
        else if ( iResult == 0 )
            printf("Connection closed\n");
        else
		{
            printf("recv failed with error: %d\n", WSAGetLastError());
			break;
		}
	} 

	while(iResult != 0);
	free(request_buf);
	//free(recvbuf);
    // cleanup
    closesocket(ConnectSocket);
    WSACleanup();

	return 0;

}

